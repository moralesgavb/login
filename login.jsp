<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="oracle.security.am.common.utilities.constant.GenericConstants"  %>
<%@page import="java.util.*, java.text.MessageFormat"%>
<%@page import="oracle.security.am.engines.sso.ServerMsg" %>
<%@page import="oracle.security.am.common.utilities.css.CSSUtil"%>
<%@page import="oracle.security.am.common.utilities.css.XSSFilter"%>
<%@ page import="oracle.security.am.pbl.util.LocaleHandler" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="oracle.ldap.util.Base64" %>
<%@ page import="oracle.security.am.common.utilities.css.XSSUtil" %>
<%@ page import="oracle.security.am.engines.idm.IdentityManagementEngine" %>
<%@ page session="false"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%!
public void jspInit(){
  oracle.security.am.pbl.transport.http.proxy.ServletCatalog.registerServlet("/pages/login.jsp", this);
}
%>

<%
  //Set the Expires and Cache Control Headers
//  response.setHeader("Cache-Control", "public, max-age=300");
  response.setHeader("Cache-Control", "no-cache, no-store");
  response.setHeader("Pragma", "no-cache");
  
  //SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss z");
  //String time=sdf.format(new Date().getTime() +300*1000L);
  //response.setHeader("Expires", time);
    response.setHeader("Expires", "0");
  //long now = System.currentTimeMillis();
  //response.setDateHeader("Expires", now + 300000);
  response.setContentType("text/html; charset=UTF-8");

  //Added for Combating ClickJacking
  XSSUtil.addHeader(response, "");
  //initializing the messageBundle first
  String defaultResourceBundle = "oracle.security.am.engines.sso.ServerMsg";
 // java.util.Locale myLocale = request.getLocale();

  
  Object[] langValues = LocaleHandler.getLocaleForDefaultLoginPage(request, response);
  //this is the default Locale
  java.util.Locale myLocale = (java.util.Locale) langValues[0];

  final String language = myLocale.getLanguage();
  final String textDirection = ("ar".equalsIgnoreCase(language) ||
              "he".equalsIgnoreCase(language) ||
              "iw".equalsIgnoreCase(language)) ? "rtl" : "ltr";
  final String cssPrefix = "ltr".equalsIgnoreCase(textDirection)?"":"_rtl" ;
  

 String lang = (String) langValues[1];
 String userLanguageList = LocaleHandler.getUserLangList();
  ResourceBundle msgBundle  = ResourceBundle.getBundle(defaultResourceBundle, myLocale);

%>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><%=msgBundle.getString("LOGIN_TITLE")%></title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/pages/assets/css/style.css">

    <!-- Start Disable frame hijacking Script-->
    <%@ include file="/pages/clickjackingScript.jsp" %>
    <!-- End Disable frame hijacking Script-->

    <script language="javascript" type="text/javascript">
        var currentPageLang = '<%=lang%>';
        var userLanguageArray = new Array( < %= userLanguageList % > );
        var isError;
    </script>
    <script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/pages/js/config.js">
    </script>
    <script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/pages/js/messages.js">
    </script>
    <script language="javascript" type="text/javascript" src="<%=request.getContextPath()%>/pages/js/loginJS.js">
    </script>

    <script language="javascript" type="text/javascript">
        function submitform() {

            document.loginData.action = "<%=request.getContextPath()%>/pages/login.jsp";
            document.loginData.submit();
            document.loginData.action = "/oam/server/auth_cred_submit";
        }
    </script>

</head>

<body onload="setFocusOnElement('username');javascript:onBodyLoad();">

    <main>

        <header class="d-flex">

            <div class="logo">
                <img src="<%=request.getContextPath()%>/pages/assets/img/logo.svg" alt="Engie logo">
            </div>

            <div class="landscape">
                <img src="<%=request.getContextPath()%>/pages/assets/img/landscape.svg" alt="Engie landscape">
            </div>

        </header>

        <section>

            <%
            String reqId  = XSSFilter.sanitizeStringInput(request.getParameter(GenericConstants.REQUEST_ID));

            if( reqId == null && request.getAttribute(GenericConstants.REQUEST_ID) != null ) {
            reqId = (String)request.getAttribute(GenericConstants.REQUEST_ID);
            }
            reqId = CSSUtil.escapeHtmlFull(reqId);

            final String registrationUrl = (String)request.getAttribute(GenericConstants.USER_REGISTRATION_URL);
            String forgotPasswordUrl = (String)request.getAttribute(GenericConstants.FORGOT_PASSWORD_URL);
            final String trackRegistrationUrl = (String)request.getAttribute(GenericConstants.TRACK_USER_REGISTRATION_URL);
            final boolean promptKeepMeSignedIn = Boolean.parseBoolean((String)request.getAttribute(GenericConstants.PROMPT_PERSISTENT_LOGIN));

            String simpleMessage = null;
            String errCode = XSSFilter.sanitizeStringInput(request.getParameter(GenericConstants.ERROR_CODE));
            String reqToken  = XSSFilter.sanitizeRequestToken(request.getParameter(GenericConstants.AM_REQUEST_TOKEN_IDENTIFIER));

            IdentityManagementEngine m_idmEngine = IdentityManagementEngine.getInstance();
            Boolean isIdentityServiceEnabled = m_idmEngine.isIdentityServiceEnabled();
            Boolean isRegistrationServiceEnabled = m_idmEngine.isRegistrationServiceEnabled();
            if(isIdentityServiceEnabled && !isRegistrationServiceEnabled){
            forgotPasswordUrl = null;
            }
            %>

            <h1><%=msgBundle.getString("WELCOME")%></h1>

            <form action="/oam/server/auth_cred_submit" method="post" name="loginData">

                <noscript>
                    <small class="error">JavaScript is required. Enable JavaScript to use WebLogic Administration Console.</small>
                </noscript>
                <small><%=msgBundle.getString("SIGN_IN")%></small>
                
                <%
                    if(errCode != null && errCode.length() > 0) {
                    try {
                        simpleMessage = msgBundle.getString(errCode);
                    } catch(Exception e) {
                        //get the default authn failed message
                        simpleMessage = msgBundle.getString("OAM-8");
                    }
                %>
                   
                <noscript>
                    <small class="error">JavaScript is required. Enable JavaScript to use WebLogic Administration Console.</small>
                </noscript>
                <small class="error"><%=simpleMessage%></small> 
        
                <%
                }
                %>

                <label for="<%=GenericConstants.USERNAME%>"><%=msgBundle.getString("USERNAME_LBL")%></label>
                <input type="text" id="<%=GenericConstants.USERNAME%>" name="<%=GenericConstants.USERNAME%>" placeholder="Your username here" autocomplete="off">

                <label for="<%=GenericConstants.PASSWORD%>"><%=msgBundle.getString("PASSWORD_LBL")%></label>
                <input type="password" id="<%=GenericConstants.PASSWORD%>" name="<%=GenericConstants.PASSWORD%>" placeholder="Your password here" autocomplete="off">

                <div class="d-flex form-footer">

                    <small>
                        <%
                        if(forgotPasswordUrl != null) {
                            // show url only if the url is not null
                        %>
                            <a href="<%=forgotPasswordUrl%>"><%=msgBundle.getString("FORGOT_PWD")%></a>
                        <%
                        }
                        if(registrationUrl != null) {
                        %>
                            <a href="<%=registrationUrl%>"><%=msgBundle.getString("REGISTER_ACCT")%></a>
                        <%
                        }
                        if(trackRegistrationUrl != null) {
                        %>
                            <a href="<%=trackRegistrationUrl%>"><%=msgBundle.getString("TRACK_REG")%></a>
                        <%
                        }
                        %>
                    </small>

                    <input type="submit" value="<%=msgBundle.getString("LOGIN")%>" onclick="this.disabled=true;document.body.style.cursor = 'wait'; this.className='formButton-disabled';form.submit();return false;">
                    <input type="hidden" name="<%=GenericConstants.REQUEST_ID%>" value="<%=reqId%>">

                </div>

            </form>

        </section>

    </main>

</body>

</html>