<%@page session="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="oracle.security.am.common.utilities.constant.GenericConstants" %>
<%@page import="java.util.*, java.text.MessageFormat, oracle.security.am.engines.sso.ServerMsg"%>
<%@ include file="locale-ctx.jsp" %>
<%@ page import="oracle.security.am.pbl.util.LocaleHandler" %>
<%@page import="oracle.security.am.common.utilities.css.XSSFilter"%>
<%@ page import="oracle.security.am.common.utilities.css.XSSUtil" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%!
public void jspInit(){
  oracle.security.am.pbl.transport.http.proxy.ServletCatalog.registerServlet("/pages/genericerror.jsp", this);
}
%>

<%
    //Set the Expires and Cache Control Headers
    response.setHeader("Cache-Control", "no-cache, no-store");
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Expires", "0");
    //Added for Combating ClickJacking
    XSSUtil.addHeader(response, "");
    response.setContentType("text/html; charset=UTF-8");
    
    //initializing the messageBundle first
    String defaultResourceBundle = "oracle.security.am.engines.sso.ServerMsg";
    // java.util.Locale myLocale = request.getLocale();
	Object[] langValues = LocaleHandler.getLocaleForDefaultLogoutPage(request, response);
	//this is the default Locale
	java.util.Locale myLocale = (java.util.Locale) langValues[0];
	String langDashCount = (String) langValues[1];

    ResourceBundle msgBundle  = ResourceBundle.getBundle(defaultResourceBundle, myLocale);
    ResourceBundle message = ResourceBundle.getBundle("otpchangepswd", myLocale);
    String logoutStatus = XSSFilter.sanitizeStringInput(request.getParameter(GenericConstants.LOGOUT_STATUS));
    String logoutMessage = "LOGOUT_SUCCESS";
    if ("1".equals(logoutStatus))
       logoutMessage = "LOGOUT_FAILURE";
    else if ("2".equals(logoutStatus)) 
       logoutMessage = "LOGOUT_PARTIAL";

%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><%=msgBundle.getString("SINGLE_SIGN_OFF")%></title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/pages/assets/css/style.css">

    <!-- Start Disable frame hijacking Script-->
    <%@ include file="/pages/clickjackingScript.jsp" %>
    <!-- End Disable frame hijacking Script-->
</head>

<body>

    <main>

        <header class="d-flex">

            <div class="logo">
                <img src="<%=request.getContextPath()%>/pages/assets/img/logo.svg" alt="Engie logo">
            </div>

            <div class="landscape">
                <img src="<%=request.getContextPath()%>/pages/assets/img/landscape.svg" alt="Engie landscape">
            </div>

        </header>

        <section>

            <h1 class="error"><%=message.getString("internal_err")%></h1>

            <p>
            <% if(request.getAttribute("INTERNAL_ERROR_MSG") != null) {%>
                Internal error Message : <%=((String)request.getAttribute("INTERNAL_ERROR_MSG"))%>
            <%}%>
            </p>

        </section>

    </main>
    
</body>

</html>